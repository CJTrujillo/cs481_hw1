//
// CJ Trujillo 9/10/20
// CS 481 HW1
// Hello World
// Displays requested information about app creator
//

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Hello World',
      home:Scaffold(
      appBar: AppBar(

        title: Text('Hello World'),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Hello World',),
            Text('by CJ Trujillo',),
            Text('Fall 2020 Graduate',),
            Text('I sneezed on the beat and the beat got sicker',),
            Text('~ Beyonce',),
          ],
        ),
        ),
      ),
    );
  }
}
